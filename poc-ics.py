# Spacy
import spacy
# Pandas
import pandas as pd 
# Dates parser
from dateutil.parser import parse

# -------------------------------------------
# --------------- Class : Gus ---------------
# -------------------------------------------

# My agent frame
class Gus:

    # Frame from frame.csv
    frame = pd.read_csv("frame.csv", sep=";") 
    # Load Spacy model package "en_core_web_sm"
    nlp = spacy.load('en_core_web_sm')
    BOTNAME = "???"

    # Main algorithm
    def run(self):
        # Choose interlocutor
        print(f"{self.BOTNAME} : Choose your interlocutor with the correct code")
        print(f"{self.BOTNAME} : #1 Clapou, #2 Fifou, #3 Rémig:")
        choice = int(input())
        if(choice == 1): self.BOTNAME = "Clapou"
        elif(choice == 2): self.BOTNAME = "Fifou"
        else: self.BOTNAME = "Rémig"
        # For each slot
        for index, row in self.frame.iterrows():
            # If slot is empty, look for
            if(pd.isna(self.frame["Answer"][index])):
                # Print the related question
                print(f"{self.BOTNAME} :", row["Question"])
                # Analyse response
                self.analyseAnswer(input(), index)
            if(row["Type"] == "DATE" and pd.notna(self.frame["Answer"][index])):
                self.frame["Answer"][index] = parse(self.frame["Answer"][index])
        # Modify part
        code = ""
        # Run while you write 999
        while code != '999':
            print("      --------------------------------")
            print(f"{self.BOTNAME} : There is a recap of your book:")
            print(f"{self.BOTNAME} : Write '999' to validate:")
            # List every slot
            # for index, row in self.frame.iterrows():
            #    print(f"{self.BOTNAME} : Code:", index, "->", row["Answer"])
            print("---")
            print(self.frame[["Question", "Answer"]])
            print("---")
            print(f"{self.BOTNAME} : Write the line (id) to modify:")
            code = input()
            # If valid code, modify strict
            if int(code) < len(self.frame):
                print("GUS : Veuillez corriger la valeur:")
                # Take the input, no filters
                self.frame["Answer"][int(code)] = input()
        print(f"{self.BOTNAME} : Thank's for booking, see you soon !")
        print("---")
        print("The final frame is :")
        # Print the final frame
        print(self.frame[["Question", "Answer"]])

    # Analyser with Spacy
    def analyseAnswer(self, sentence, frameIndex):
        # Suppress the warning of copy ignore it
        pd.options.mode.chained_assignment = None
        # If just one word, take it and don't check
        if(len(sentence.split(" ")) == 1): self.frame["Answer"][frameIndex] = sentence
        elif(self.frame["Type"][frameIndex] == "OTHER"):
            self.frame["Answer"][frameIndex] = sentence
        # Else check
        else:
            # Use Spacy to Tokenize and Tag
            doc = self.nlp(sentence)
            # For each named entities
            for entity in doc.ents:
                # Iterate into slots
                for index, row in self.frame.iterrows():
                    # If types match
                    if(entity.label_ == row["Type"] and pd.isna(row["Answer"])):
                        # Save and break
                        self.frame["Answer"][index] = entity.text
                        break

# -----------------------------------------
# --------------- Main code ---------------
# -----------------------------------------

# Create new Gus
myAgent = Gus()
# Run
myAgent.run()
