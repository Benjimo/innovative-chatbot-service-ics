# POC : Innovative Chatbot Service
The goal of this POC is to create the Gus like chatbot in Python. The chatbot is a fake interlocutor, who can ask you some questions defined in a frame to book a camping. The chatbot don't ask you some question if he detects the answer on an other question. This chatbot is based on this algorithm :

>  ### You maybe need to install Spacy on your computer to run correctly the code.
> `pip install spacy`
> `pip install pandas`
> `python -m spacy download en_core_web_sm`

![Shem](./images/shem.png)
*Source: https://web.stanford.edu/class/cs124/lec/chatbot.pdf*

## PEAS and environment properties
- Performance : Book your camping with a minimum number of errors
- Environment : Chat, terminal
- Actuators : Responses, frame and slot, matching rules
- Sensors : Terminal, keyboard inputs

| Completely observable | Deterministic | Episodic | Static | Discreet | Single agent |
|--|--|--|--|--|--|
| Yes. The agent has access to all informations that the user give. | Yes. The answer depend of the action the chatbot choose, based on a ruleset and API. | No. In our chatbot program we have a notion of memory with frames and slots | Yes. The state cannot change during the response time. | Yes. The number of slots. | No. It's a multi agent because my chatbot is based on API from Spacy for example. |

## What can my chatbot do ?
- ICS chatbot ask maximum the number of questions given in the (csv)[./frame.csv] to book your flight and save the result on a frame
- If your answer respond to two or more slots at once, he don't ask you the question and use the previous response
- The frame architecture is stored in a CSV file, you can edit it with excel and export in CSV **(with ';' separators)**
- Our chatbot is based on Spacy so you can write sentences, he will detect named entities like *GPE* for `From Paris to Nice`
- I use dateutil library for the date so you can have more flexibility like write this `I would like to leave monday 14 and arrive the 2020-12-15`, he will detect *2020-12-14 00:00:00* and *2020-12-15 00:00:00*
- You can modify (force) your book after the last question with a code

## Example
![Example](./images/result.png)
